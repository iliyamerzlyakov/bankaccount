import java.util.Scanner;
/**
 * Класс для представления работоспособности Account
 *
 * @author И. Мерзляков
 */
public class Demo {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        final Account SBERBANK = new Account( "001", "Merzlyakov Iliya" );
        final Account VTB = new Account( "002", "Merzlyakov Iliya" );

        final Account.Card SB_VISA = SBERBANK.new Card( "7639 6334 6348 2531" );
        final Account.Card SB_MIR = SBERBANK.new Card( "4812 0928 4825 2834" );
        final Account.Card VTB_MASTERCARD = VTB.new Card( "8734 6283 2742 3682" );

        Account.Card[] cards = {SB_MIR, SB_VISA, VTB_MASTERCARD};
        Account.Card card = cardSelection( cards );


        cardSelection( cards );
        printMenu();
        switchMenu( card, cards );
    }

    /**
     * Поиск аккаунта по номеру карты
     *
     * @param cards массив карт
     * @return
     */
    private static Account.Card cardSelection(Account.Card[] cards) {
        System.out.print( "Втавьте карту: " );
        //Ввод номера карты
        String num = scanner.nextLine();
        for (Account.Card card : cards) {
            if (num.equals( card.getNumber() )) {
                return card;
            } else {
                System.out.println( "Карта не найдена.\n" + "Операция завершена." );
            }
        }
        return null;
    }

    /**
     * Меню аккаунта
     */
    private static void printMenu() {
        System.out.println( "" + "Нажмите:\n" + "1 - просмотр баланса\n" + "2 - пополнение\n" + "3 - снятие\n" + "4 - смена карты\n" + "5 - выход" );
    }

    /**
     * SwitchMenu(меню)
     *
     * @param card  карта, с которой работает пользователь
     * @param cards массив карт
     */
    private static void switchMenu(Account.Card card, Account.Card[] cards) {
        String num = scanner.next();
        switch (num) {
            case "1":
                System.out.print( "На вашем счету: " );
                break;
            case "2":
                System.out.print( "Введите сумму для пополнения баланса: " + card.deposit( scanner.nextInt() ) );
                break;
            case "3":
                System.out.print( "Введите сумму для снятия с баланса: " + card.withdraw( scanner.nextInt() ) );
                break;
            case "4":
                System.out.println( cardSelection( cards ) );
                break;
            case "5":
                System.out.print( "Выход" );
                System.exit( 0 );
        }
        switchMenu( card, cards );
    }
}



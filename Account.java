public class Account {
    private final String number;
    private final String owner;
    private long amount;

    Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    Account(final String number, final String owner, long amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    String getNumber() {
        return number;
    }

    String getOwner() {
        return owner;
    }

    long getAmount() {
        return amount;
    }

    /**
     * Метод, реализующий снятие средств с карты
     * @param amountToWithdraw (сумма снятия)
     * @return Возвращает либо 0, если сумма для снятия отрицательная, либо, если сумма снятия больше баланса, возвращает максимально возможную сумму, которую можно снять. В других случаях возвращает сумму снятия
     */
    private long withdraw(long amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final long amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        return amountToWithdraw;
    }

    /**
     * Метод, реализующий зачисление средств на карту
     * @param amountToInput (сумма зачисления)
     * @return Возвращает либо 0, если сумма зачисления отрицательная, либо баланс, которому прибавили сумму зачисления
     */
    private long deposit(long amountToInput) {
        if (amountToInput < 0) {
            return 0;
        }
        return amount += amountToInput;
    }

    /**
     * Внутренний класс Card
     */
    class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        String getNumber() {
            return number;
        }


        long withdraw(final long amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        long deposit(final long amountToDeposit) {
            return Account.this.deposit(amountToDeposit);
        }
    }

}

